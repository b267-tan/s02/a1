<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Activity s02</title>
	</head>
	<body>
		<h2>Divisible By Five</h2>
		<p><?php printDivisibleByFive()?></p>

		<h2>Array Manipulation</h2>
		<!-- Add a student -->
		<?php addStudent("John Smith");?>
		<pre><?php print_r($students);?></pre>

		<!-- Count the number of students -->
		<pre><?php echo count($students)?></pre>

		<!-- Add another student and print count -->
		<?php addStudent("Jane Smith");?>
		<pre><?php print_r($students);?></pre>
		<pre><?php echo count($students)?></pre>

		<!-- Remove the first student and print count -->
		<?php removeStudent();?>
		<pre><?php print_r($students);?></pre>
		<pre><?php echo count($students)?></pre>
	</body>
</html>