<?php

function printDivisibleByFive(){
	for($count = 0; $count <= 1000; $count++){
			if($count % 5 !== 0){
				continue;
			}
			echo $count. " ";
			if($count >= 100) {
				break;
			}
		}
}


$students = [];

function addStudent($student){
	global $students;
	array_push($students, $student);
}

function removeStudent(){
	global $students;
	array_shift($students);
}